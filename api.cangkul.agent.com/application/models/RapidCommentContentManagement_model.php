<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RapidCommentContentManagement_model extends CI_Model
{

    protected $db_table_comment = 'rapid_comment_content';

    protected $db_api = null;
    protected $client_access_key = null;
    public function __construct(){
        parent::__construct();
        $this->db_api = $this->load->database("openrapid_api_comment", TRUE);
    }   
    public function set_client_access_key($key){
        $this->client_access_key = $key;
    }
    public function get_columns(){
        $query = $this->db->query("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '".$this->db_table_comment."' ");
        $columns = array();
        foreach ($query->result() as $key => $value) {
            $columns[$value->COLUMN_NAME] = $value;
            unset($columns["client_access_key"]);
            unset($columns["id"]);
            unset($columns["reply_to_id"]);
        }
        return $columns;
    }
    public function read_once($table, $args = array()){
        $this->db_api->select("*");
        $this->db_api->from($table);
        $this->db_api->where("client_access_key", $this->client_access_key);
        $this->db_api->where($args["key"], $args["value"]);
        return $this->db_api->get()->row();
    }
    public function read_comment($args = array()){
        if (!empty($args["data"])) {
            $this->db_api->select($args["data"]);
        }else{
            $this->db_api->select("
               id,
               reply_to_id,
               container_id,
               text,
               username,
               picture,
               email,
               website,
               date_created,
               is_published
            ");
        }
        $this->db_api->from($this->db_table_comment);
        $this->db_api->where("client_access_key", $this->client_access_key);
        if (!empty($args["use_filter"]["id"])) {
            $this->db_api->where("id", $args["use_filter"]["id"]);
        }
        if (!empty($args["use_filter"]["container_id"])) {
            $this->db_api->where("container_id", $args["use_filter"]["container_id"]);
        }
        if (!empty($args["parent_comment"])) {
            $this->db_api->where("reply_to_id", "");
        }
        if (!empty($args["parent_comment_id"])) {
            $this->db_api->where("reply_to_id", $args["parent_comment_id"]);
        }
        if (!empty($args["is_published"])) {
            $this->db_api->where("is_published", $args["is_published"]);
        }
        if (!empty($args["limit"])) {
            if (!empty($args["start"])) {
                $this->db_api->limit($args["limit"], $args["start"]);
            }else{
                $this->db_api->limit($args["limit"], 0);
            }
        }
        if (!empty($args["use_filter"]["id"])) {
            return $this->db_api->get()->row();
        }else{
            return $this->db_api->get()->result_array();
        }
    }
    public function create_comment($data){
        return $this->db_api->insert($this->db_table_comment, $data);
    }
    public function delete_comment($id){
        return $this->db_api->delete($this->db_table_comment, array("id" => $id));
    }
    public function delete_comment_by_container($id){
        return $this->db_api->delete($this->db_table_comment, array("container_id" => $id));
    }
    public function update_comment($id, $data){
        return $this->db_api->update($this->db_table_comment, $data, array('id' => $id));
    }
}


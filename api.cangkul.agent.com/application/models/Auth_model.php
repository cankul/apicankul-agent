<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model
{
	protected $db_client = null;
    protected $_table = 'clients';
    public function __consturct(){
    	parent::__consturct();
    }
    public function verify_client($client_s_key){
    	$db_client = $this->load->database('db_client', TRUE);
        $db_client->select("count(*) verified");
        $db_client->from("clients");
        $db_client->where("client_secret_key", $client_s_key);
        if ($db_client->get()->row("verified") > 0) {
        	return array("verified" => true);
        }else{
        	return  array("verified" => false);
        }
    }
}


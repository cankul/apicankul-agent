<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RapidBlogContentManagement_model extends CI_Model
{

    protected $db_table_content = 'rapid_blog_content';
    protected $db_table_content_category = 'rapid_blog_content_category';
    protected $db_table_content_subcategory = 'rapid_blog_content_subcategory';
    protected $db_table_content_tag = 'rapid_blog_content_tag';

    protected $db_api = null;
    protected $client_access_key = null;
    public function __construct(){
        parent::__construct();
        $this->db_api = $this->load->database("openrapid_api_blog", TRUE);
    }   
    public function set_client_access_key($key){
        $this->client_access_key = $key;
    }
    public function read_once($table, $c_id){
        $this->db_api->select("*");
        $this->db_api->from($table);
        $this->db_api->where("id", $c_id);
        return $this->db_api->get()->row();
    }

    public function create($data)
    {
        return $this->db_api->insert($this->db_table_content, $data);
    }
    public function create_tag($data){
        return $this->db_api->insert($this->db_table_content_tag, $data);
    }
    public function check_duplicate_title($title){
        $this->db_api->select("count(*) as MatchFound");
        $this->db_api->from($this->db_table_content);
        $this->db_api->where("title", $title);
        if ($this->db_api->get()->row()->MatchFound > 0) {
            return true;
        }else{
            return false;
        }
    }
    public function read_content($args = array()){
        if (!empty($args["data"])) {
            $this->db_api->select($args["data"]);
        }else{
            $this->db_api->select("
                id, 
                date_created, 
                thumbnail, 
                title, 
                description, 
                content, 
                content_likes, 
                content_views, 
                content_reff, 
                publisher_id, 
                is_published, 
                content_custom_url,
                category_id, 
                subcategory_id
            ");
        }
        $this->db_api->from($this->db_table_content);
        if (!empty($args["is_published"])) {
            $this->db_api->where("is_published", $args["is_published"]);
        }
        if (!empty($args["use_filter"])) {
            if (!empty($args["use_filter"]["category_id"])) {
                $this->db_api->where("category_id", $args["use_filter"]["category_id"]);
            }
            if (!empty($args["use_filter"]["subcategory_id"])) {
                $this->db_api->where("subcategory_id", $args["use_filter"]["subcategory_id"]);
            }
        }
        if (!empty($args["limit"])) {
            if (!empty($args["start"])) {
                $this->db_api->limit($args["limit"], $args["start"]);
            }else{
                $this->db_api->limit($args["limit"], 0);
            }
        }
        return $this->db_api->get()->result_array();
    }
     public function search_content($args = array()){
        $this->db_api->distinct();
        if (!empty($args["data"])) {
            $this->db_api->select($args["data"]);
        }else{
            $this->db_api->select("content_id");
        }
        $this->db_api->from($this->db_table_content_tag);
        $this->db_api->like("name", $args["query"]);
        $this->db_api->where("client_access_key", $this->client_access_key);
        // $this->db_api->join("content", "content.id = content_tag.content_id");
        if (!empty($args["is_published"])) {
            $this->db_api->where("is_published", $args["is_published"]);
        }
        if (!empty($args["limit"])) {
            if (!empty($args["start"])) {
                $this->db_api->limit($args["limit"], $args["start"]);
            }else{
                $this->db_api->limit($args["limit"], 0);
            }
        }
        return $this->db_api->get()->result_array();
    }
    public function read_content_tag($args = array()){
        $this->db_api->select("id, name, content_id");
        $this->db_api->from($this->db_table_content_tag);
        $this->db_api->where("content_id", $args["content_id"]);
        return $this->db_api->get()->result_array();
    }
    public function read_content_once($c_id){
        $this->db_api->select("
                id, 
                date_created, 
                thumbnail, 
                title, 
                description, 
                content, 
                content_likes, 
                content_views, 
                content_reff, 
                publisher_id, 
                is_published, 
                content_custom_url,
                category_id, 
                subcategory_id
        ");
        $this->db_api->from($this->db_table_content);
        $this->db_api->where("id", $c_id);
        $this->db_api->where("client_access_key", $this->client_access_key);
        return $this->db_api->get()->row();
    }
       public function delete_content($id){
        return $this->db_api->delete($this->db_table_content, array("id" => $id));
    }
    public function update_content($id, $data){
        return $this->db_api->update($this->db_table_content, $data, array('id' => $id));
    }

    // Category 
    public function find_duplicate_category_name($name){
        $this->db_api->select("count(*) as MatchFound");
        $this->db_api->from($this->db_table_content_category);
        $this->db_api->where("name", $name);
        if ($this->db_api->get()->row()->MatchFound > 0) {
            return true;
        }else{
            return false;
        }
    }
     public function find_duplicate_subcategory_name($name){
        $this->db_api->select("count(*) as MatchFound");
        $this->db_api->from($this->db_table_content_subcategory);
        $this->db_api->where("name", $name);
        if ($this->db_api->get()->row()->MatchFound > 0) {
            return true;
        }else{
            return false;
        }
    }
    public function read_category($args = array()){
        if (!empty($args["data"])) {
            $this->db_api->select($args["data"]);
        }else{
            $this->db_api->select("id, name, thumbnail, is_published");
        }
        $this->db_api->from($this->db_table_content_category);
        if (!empty($args["use_filter"]["id"])) {
            $this->db_api->where("id", $args["use_filter"]["id"]);
        }
        if (!empty($args["is_published"])) {
            $this->db_api->where("is_published", $args["is_published"]);
        }
        if (!empty($args["limit"])) {
            if (!empty($args["start"])) {
                $this->db_api->limit($args["limit"], $args["start"]);
            }else{
                $this->db_api->limit($args["limit"], 0);
            }
        }
        return $this->db_api->get()->result_array();
    }
    public function read_category_once($args = array()){
        if (!empty($args["data"])) {
            $this->db_api->select($args["data"]);
        }else{
            $this->db_api->select("id, name, thumbnail, is_published");
        }
        $this->db_api->from($this->db_table_content_category);
        $this->db_api->where("id", $args["id"]);
        $this->db_api->where("client_access_key", $this->client_access_key);
        if (!empty($args["is_published"])) {
            $this->db_api->where("is_published", $args["is_published"]);
        }
        return $this->db_api->get()->row();
    }
    public function read_subcategory($args = array()){
        if (!empty($args["data"])) {
            $this->db_api->select($args["data"]);
        }else{
            $this->db_api->select("id, name, thumbnail, is_published, category_id");
        }
        $this->db_api->from($this->db_table_content_subcategory);
        $this->db_api->where("client_access_key", $this->client_access_key);
        
        if (!empty($args["use_filter"]["id"])) {
            $this->db_api->where("id", $args["use_filter"]["id"]);
        }
        if (!empty($args["use_filter"]["category_id"])) {
            $this->db_api->where("category_id", $args["use_filter"]["category_id"]);
        }
        if (!empty($args["is_published"])) {
            $this->db_api->where("is_published", $args["is_published"]);
        }
        if (!empty($args["limit"])) {
            if (!empty($args["start"])) {
                $this->db_api->limit($args["limit"], $args["start"]);
            }else{
                $this->db_api->limit($args["limit"], 0);
            }
        }
        return $this->db_api->get()->result_array();
    }
    public function read_subcategory_once($args = array()){
        if (!empty($args["data"])) {
            $this->db_api->select($args["data"]);
        }else{
            $this->db_api->select("id, name, thumbnail, is_published, category_id");
        }
        $this->db_api->from($this->db_table_content_subcategory);
        if (!empty($args["use_filter"]["id"])) {
            $this->db_api->where("id", $args["use_filter"]["id"]);
        }
        if (!empty($args["use_filter"]["category_id"])) {
            $this->db_api->where("category_id", $args["use_filter"]["category_id"]);
        }
        if (!empty($args["is_published"])) {
            $this->db_api->where("is_published", $args["is_published"]);
        }
        return $this->db_api->get()->row();
    }
    public function create_category($data){
        $this->db_api->insert($this->db_table_content_category, $data);
        return $this->db_api->insert_id();
    }
     public function create_subcategory($data){
        return $this->db_api->insert($this->db_table_content_subcategory, $data);
    }
    public function delete_category($id){
        return $this->db_api->delete($this->db_table_content_category, array("id" => $id));
    }
    public function delete_subcategory($id){
        return $this->db_api->delete($this->db_table_content_subcategory, array("id" => $id));
    }
    public function update_category($id, $data){
        return $this->db_api->update($this->db_table_content_category, $data, array('id' => $id));
    }
    public function update_subcategory($id, $data){
        return $this->db_api->update($this->db_table_content_subcategory, $data, array('id' => $id));
    }
}


<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class System_model extends CI_Model
{

    protected $db_table_system = 'OR_system';
    protected $db_table_system_tracker = 'OR_system_tracker';

    protected $db_api = null;
    protected $client_access_key = null;
    public function __construct(){
        parent::__construct();
    }   
    public function set_client_access_key($key){
        $this->client_access_key = $key;
    }
    public function get_error_message($code){
        $this->db->select("name, detail");
        $this->db->from($this->db_table_system);
        $this->db->where("code", $code);
        return $this->db->get()->row();
    }
    public function put_tracker($data){
        $this->db->insert($this->db_table_system_tracker, $data);
    }
}


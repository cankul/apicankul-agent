<?php  
    require_once APPPATH .'libraries/twilio/vendor/autoload.php'; 
    use Twilio\Rest\Client;
	class OtpControl extends Core{
		
		public function __construct(){
			parent::__construct();
		}

		public
			static
				function
					__Test(){
						echo "Test coming from". __CLASS__;
					}

        private 
            static
                function
                    internationalize_phone_number($phone){
                        $indonesia_id = 62;
                        $phone_first_digit = substr($phone, 0,1);
                        if ($phone_first_digit == 0) {
                            $phone_number = substr($phone, 1);
                            return $indonesia_id . $phone_number;
                        }else{
                            return $phone;
                        }
                    }

        public 
            static
                function
                    __Send_Short_Message($phone, $message){
                        $sid = "AC06e85a2c1253bbae9e1aa3c06ce70d1e";
                        $token = "b410a1af9daab8d510f71a737157d3f8";
                        try {
                            $twilio = new Client($sid, $token);
                            $send_SMS = $twilio->messages->create("+". self::internationalize_phone_number($phone) , array(
                                "From" =>   "+12173866795",
                                "Body" =>  $message
                            ));  
                           if ($send_SMS) {
                                return true;
                           }else{
                                return false;
                           }
                        } catch (Exception $e) {
                            return false;
                        }
                    }

		public
			static
				function 
					_Send(){
                        if (Core::__Required_params(array("mobile"))) {
                            $numbers = "0987654321";
                            $charUppercase = "QWERTYUIOPASDFGHJKLZXCVBNM";
                            $otp_expired = date("Y-m-d H:i:s", strtotime("+ 5 minutes"));
                            $otp = substr(str_shuffle(str_shuffle($numbers.$charUppercase)), 0, 5);
                            $message = "Your OTP Verification code is " . $otp. ", Expired at ". $otp_expired;

                            $is_exist = RapidDataModel::read("sms_otp", [
                                "where" => [
                                    "phone" => Core::__Body_Request()["mobile"]
                                ]
                            ])["rows"];

                            // If data sms is not exist in database
                            if (empty($is_exist)) {
                                // Insert data to database
                                $otp_in_database = RapidDataModel::insert('sms_otp', [
                                    "phone" => Core::__Body_Request()["mobile"],
                                    "code" => $otp,
                                    "date_expired" => $otp_expired
                                ]);
                                // If its inserted
                                if ($otp_in_database) {
                                    // Send SMS
                                    if (self::__Send_Short_Message(Core::__Body_Request()["mobile"], $message) == true) {
                                        Core::__Send_Output([
                                            "status" => true,
                                            "message" => $message,
                                            "data" => [
                                                "otp" => $otp,
                                                "code" => $otp_expired
                                            ]
                                        ]);
                                    }else{
                                        Core::__Send_Output([
                                            "status" => false,
                                            "message" => "Failed sending message, Development Mode, Trial Account, Cannot send to unverified numbers.",
                                        ]);
                                    }
                                }
                            }else{
                                // if data already exist
                                // Means that user already send OTP to their phone
                                $sms = RapidDataModel::read('sms_otp', [
                                    "where" => [
                                        "phone" => Core::__Body_Request()["mobile"]
                                    ]
                                ]);
                                // Check wether is not expired anymore
                                // If its expired
                                if (!empty($sms["rows"])) {
                                    if (strtotime(date("Y-m-d H:i:s")) > strtotime($sms["rows"][0]["date_expired"])) {
                                        // Update sms data
                                        // renew otp code and date_expired
                                        $renew = RapidDataModel::update('sms_otp', [
                                            "key" => ["phone" => Core::__Body_Request()["mobile"]],
                                            "data" => [
                                                "code" => $otp,
                                                "date_expired" => date("Y-m-d H:i:s", strtotime("+ 5 minutes"))
                                            ]
                                        ]);
                                        if ($renew) {
                                            // Send otp
                                            if (self::__Send_Short_Message(Core::__Body_Request()["mobile"], $message) == true) {
                                                Core::__Send_Output([
                                                    "status" => true,
                                                    "message" => $message,
                                                    "data" => [
                                                        "otp" => $otp,
                                                        "code" => $otp_expired
                                                    ]
                                                ]);
                                            }else{
                                                Core::__Send_Output([
                                                    "status" => false,
                                                    "message" => "Failed sending message, Development Mode, Trial Account, Cannot send to unverified numbers.",
                                                ]);
                                            }
                                        }
                                    }else{
                                        // Send response otp is sent and cant send more otp in 5 minutes
                                        Core::__Send_Output([
                                            "status" => false,
                                            "message" => "Failed, OTP Verification code already sent to this number",
                                            "date_expired" => $otp_expired
                                        ]);
                                    }
                                }else{
                                    // Send one sms in case database failed creating row
                                    $otp_in_database = RapidDataModel::insert('sms_otp', [
                                        "phone" => Core::__Body_Request()["mobile"],
                                        "code" => $otp,
                                        "date_expired" => $otp_expired
                                    ]);
                                    // If its inserted
                                    if ($otp_in_database) {
                                        // Send SMS
                                        self::__Send_Short_Message(Core::__Body_Request()["mobile"], $message);
                                        Core::__Send_Output([
                                            "status" => true,
                                            "message" => $message,
                                            "data" => [
                                                "otp" => $otp,
                                                "code" => $otp_expired
                                            ]
                                        ]);
                                    }
                                }
                            }
                        }
                    }
        public
            static
                function
                    _verify(){
                        if (Core::__Required_params(["mobile", "code"])) {
                            $verify_otp = RapidDataModel::read('sms_otp', [
                                "select" => "COUNT(*) as Matched",
                                "where" => [
                                    "phone" => Core::__Body_Request()["mobile"],
                                    "code" => Core::__Body_Request()["code"],
                                ]
                            ])["rows"][0]["Matched"];

                            if ($verify_otp > 0) {
                                $delete = RapidDataModel::delete('sms_otp', array(
                                    "key" => ["phone" => Core::__Body_Request()["mobile"]]
                                ));
                                if ($delete) {
                                    Core::__Send_Output([
                                        "status" => true,
                                        "message" => "Sucessfully verified OTP Code"
                                    ]);
                                }else{
                                    Core::__Send_Output([
                                        "status" => false,
                                        "message" => "Failed while trying to verify OTP Code"
                                    ]);
                                }
                            }else{
                                Core::__Send_Output([
                                    "status" => false,
                                    "message" => "Failed, OTP does not matched nor exist",
                                ]);
                            }
                        }
                    }
	}
  
<?php  
	class UserControl extends Core{

		protected static $_user_register_id;

		public function __construct(){
			parent::__construct();
		}

		public
			static
				function
					__Test(){
						echo "Test coming from". __CLASS__;
					}

		private 
			static
				function
					localize_phone_number($phone){
						$indonesia_id = 62;
						$phone_first_digit = substr($phone, 0,1);
						$phone_number = substr($phone, 1);
						if ($phone_first_digit == 0) {
							return $indonesia_id . $phone_number;
						}else{
							return $phone;
						}
					}

		private 
			static
				function
					__Create_Data_Borrower(){
						if (!empty(Core::__Body_Request()["password"])) {
							$password = md5(Core::__Body_Request()["password"]);
						}else{
							$password = "";
						}
						$create_data = RapidDataModel::create("borrowers", [
							[
								"user_id" => Core::__Body_Request()["user_id"] ?? "",
								"first_name" => Core::__Body_Request()["username"] ?? "",
								"last_name" => Core::__Body_Request()["last_name"] ?? "",
								"gender" => Core::__Body_Request()["gender"] ?? "",
								"title" => Core::__Body_Request()["title"] ?? "",
								"mobile" => Core::__Body_Request()["mobile"],
								"email" => Core::__Body_Request()["email"] ?? "",
								"unique_number" => Core::__Body_Request()["mobile"],
								"dob" => Core::__Body_Request()["dob"] ?? "",
								"address" => Core::__Body_Request()["address"] ?? "",
								"city" => Core::__Body_Request()["city"] ?? "",
								"state" => Core::__Body_Request()["state"] ?? "",
								"zip" => Core::__Body_Request()["zip"] ?? "",
								"phone" => Core::__Body_Request()["phone"] ?? Core::__Body_Request()["mobile"],
								"business_name" => Core::__Body_Request()["business_name"] ?? "",
								"working_status" => Core::__Body_Request()["working_status"] ?? "",
								"photo" => Core::__Body_Request()["photo"] ?? "",
								"photo_id_card" => Core::__Body_Request()["photo_id_card"] ?? "",
								"photo_with_id_card" => Core::__Body_Request()["photo_with_id_card"] ?? "",
								"notes" => Core::__Body_Request()["notes"] ?? "",
								"files" => Core::__Body_Request()["files"] ?? "",
								"loan_officers" => Core::__Body_Request()["loan_officers"] ?? "",
								"created_at" => date("Y-m-d H:i:s"),
								"updated_at" => date("Y-m-d H:i:s"),
								"deleted_at" => NULL,
								"username" => Core::__Body_Request()["username"] ?? Core::__Body_Request()["mobile"],
								"password" => $password,
								"month" => date("m"),
								"year" => date("Y"),
								"source" => "online",
								"blacklisted" => 0,
								"branch_id" => Core::__Body_Request()["branch_id"] ?? 1,
								"country_id" => Core::__Body_Request()["country_id"] ?? 102
							]
						]);
						if (!empty($create_data)) {
							self::$_user_register_id = $create_data;
							return true;
						}else{
							return false;
						}
					}
		private
			static 
				function
					__Create_Data_Borrowers_Group(){
						$create_borrower_groups = RapidDataModel::create('borrower_group_members', array(
							[
								"borrower_group_id" => Core::__Body_Request()["borrower_group_id"],
								"borrower_id" => self::$_user_register_id,
								"created_at" => date("Y-m-d H:i:s"),
								"updated_at" => date("Y-m-d H:i:s")
							]
						));
						if (!empty($create_borrower_groups)) {
							return true;
						}else{
							return false;
						}
					}
		public
			static
				function
					_Register_Password(){
						if (Core::__Required_Params(["mobile", "password"])) {
							$update = RapidDataModel::update('borrowers', [
								"key" => ["mobile" => Core::__Body_Request()["mobile"]],
								"data" => [
									"password" => md5(Core::__Body_Request()["password"])
								]
							]);
							if ($update) {
									$token = self::__Create_User_Token(Core::__Body_Request()["mobile"]);
									$set_token = RapidDataModel::insert('borrowers_token', [
										"mobile" => Core::__Body_Request()["mobile"],
										"token" => $token,
									]);
									if ($set_token) {
										$borrower = RapidDataModel::read("borrowers", [
											"select" => "id",
											"where" => [
												"mobile" => Core::__Body_Request()["mobile"]
											]
										])["rows"];
										if (!empty($borrower)) {
											Core::__Send_Output([
												"status" => true,
												"message" => "Succesfully signup",
												"token" => $token,
												"id" => $borrower[0]["id"]
											]);
										}else{
											Core::__Send_Output(array(
												"status" => false,
												"message" => "Failed while trying to signup"
			 								));
										}
									}else{
										Core::__Send_Output(array(
											"status" => false,
											"message" => "Failed while trying to create token"
		 								));
									}
							}else{
								Core::__Send_Output(array(
									"status" => false,
									"message" => "Failed while registering user"
		 						));
							}
						}
					}
		public
			static
				function 
					_Register(){
						// Register new user
						$required_params_register = ["mobile"];
						if (Core::__Required_Params($required_params_register)) {
							$phone_exist = RapidDataModel::is_exist('borrowers', ["mobile" => Core::__Body_Request()["mobile"]]);
							if ($phone_exist) {
								Core::__Send_Output([
									"status" => false,
									"message" => API_FAILED_INSERT_DATABASE,
									"message_error" => "A user with this phone number already registered"
								]);
							}else{
								if (self::__Create_Data_Borrower()) {
									Core::__Send_Output([
										"status" => true,
										"message" => "User succesfully registered",
									]);
								}else{
									Core::__Send_Output([
										"status" => false,
										"message" => API_FAILED_INSERT_DATABASE,
									]);
								}
							}
						}
					}
		private
			static
				function
					__Create_User_Token($phone){
						return base64_encode(json_encode([
							"unique_id" => uniqid(),
							"phone" => $phone,
							"date_created" => date("Y-m-d H:i:s"),
						]));
					}
		public
			static
				function
					_Login(){
						// Login user
						// Check required parameter
						if (Core::__Required_Params(["mobile", "password"])) {
							$login = RapidDataModel::read('borrowers', array(
								"select" => "borrowers.id, borrowers.first_name, borrowers.last_name, borrowers.gender, borrowers.title, borrowers.mobile, borrowers.phone, borrowers.email, borrowers.unique_number, borrowers.business_name, borrowers.working_status, borrowers.username, borrowers.active, borrowers.blacklisted, borrowers.branch_id, borrowers.country_id",
								"join" => [
									"borrowers_agent" => "borrowers_agent.borrower_id = borrowers.id"
								],
								"where" => array(
									"mobile" => self::localize_phone_number(Core::__Body_Request()["mobile"]),
									"password" => md5(Core::__Body_Request()["password"]),
								)
							))["rows"];

							// Login Success
							if (count($login) > 0) {
								$token = Self::__Create_User_Token(Core::__Body_Request()["mobile"]);
								// If user token exist
								if (RapidDataModel::is_exist('borrowers_token', ["mobile" => Core::__Body_Request()["mobile"]])) {
									// Update Token
									$update_token = RapidDataModel::update('borrowers_token', [
										"key" => [
											"mobile" => Core::__Body_Request()["mobile"],
										],
										"data" => [
											"token" => $token
										] 
									]);
									if ($update_token) {
										Core::__Send_Output(array(
											"status" => true,
											"message" => "Login Success",
											"token" => $token,
											"data" => $login[0]
		 								));
									}else{
										Core::__Send_Output(array(
											"status" => false,
											"message" => "Failed while trying to update token"
		 								));
									}
								}else{
									$set_token = RapidDataModel::insert('borrowers_token', [
										"mobile" => Core::__Body_Request()["mobile"],
										"token" => $token,
									]);
									if ($set_token) {
										Core::__Send_Output(array(
											"status" => true,
											"message" => "Login Success",
											"token" => $token,
											"data" => $login[0]
		 								));
									}else{
										Core::__Send_Output(array(
											"status" => false,
											"message" => "Failed while trying to create token"
		 								));
									}
								}								
							}else{
								Core::__Send_Output(array(
									"status" => false,
									"message" => API_FAILED_LOGIN,
									"message_error" => "Wrong phone, email or password",
 								));
							}
						}
					}
			public
				static
					function
						_Profile(){
							$read = [
								"select" => "*, borrowers.id as id",
								"join" => array(
									"borrower_group_members" => "borrowers.id = borrower_group_members.borrower_id"
								),
							];
							if (!empty(Core::__Body_Request()["id"])) {
								$read["where"] =  ["borrowers.id" => Core::__Body_Request()["id"]];
								$read_data = RapidDataModel::read('borrowers', $read);
								Core::__Send_Output([
									"status" => true,
									"data" => $read_data["rows"][0]
								]);
							}else{
								$read_data = RapidDataModel::read('borrowers', $read);
								Core::__Send_Output([
									"status" => true,
									"data" => $read_data["rows"]
								]);
							}
						}
			public
				static
					function
						_Profile_Update(){
							if (Core::__Required_Params(array("id", "data"))) {
								RapidDataModel::table('borrowers');
								$available_fields = RapidDataModel::read_fields();
								$data_to_update = [];
								$NOT_updateable_data = ["id", "username", "blacklisted"];
								$data_not_allowed_to_update = [];
								// Loop through body data request
								foreach (Core::__Body_Request()["data"] as $key => $value) {
									// If data key is in acceptable fields
									if (in_array($key, $available_fields)) {
										if (in_array($key, $NOT_updateable_data)) {
											$data_not_allowed_to_update[] = $key;
										}else{
											// Store data to variable
											$data_to_update[$key] = $value;
										}
									}else{
										// Store data to not updatable variable
										$data_not_allowed_to_update[] = $key;
									}
								}
								// If not allowed to update data parameter is empty
								if (empty($data_not_allowed_to_update)) {
									// And if the data to update ! empty
									if (!empty($data_to_update)) {
										// Proceed update data
										$update = RapidDataModel::update('borrowers', [
											"key" => ["id" => Core::__Body_Request()["id"]],
											"data" => $data_to_update
										]);
										if ($update) {
											Core::__Send_Output([
												"status" => true,
												"message" => "Data succesfully updated",
												"data" => $data_to_update
											]);
										}else{
											Core::__Send_Output([
												"status" => false,
												"message" => API_FAILED_INSERT_DATABASE,
											]);
										}
									}else{
										// Return response parameter data should not be empty
										Core::__Send_Output([
											"status" => false,
											"message" => API_FAILED_PARAMETER,
											"message_error" => "Parameter data should not be empty",
										]);
									}
								}else{
									// Return response data is not allowed to update
									Core::__Send_Output([
										"status" => false,
										"message" => API_FAILED_PARAMETER,
										"message_error" => "Parameter (" . implode(", ", $data_not_allowed_to_update) . ") is not updateable",
									]);		
								}
							}
						}

			public
				static 
					function
						_Forgot_Password(){
							if (Core::__Required_Params(["mobile"])) {
								$check_phone = RapidDataModel::read("borrowers", [
									"select" => "password",
									"where" => [
										"mobile" => Core::__Body_Request()["mobile"]
									]
								])["rows"];
								if (count($check_phone) > 0) {
									if (!empty(Core::__Body_Request()["renew_password"])) {
										$password = substr(str_shuffle("1234567890QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm@$!"), 0, 6);
										$update_password = RapidDataModel::update("borrowers", [
											"key" => [
												"mobile" => Core::__Body_Request()["mobile"]
											],
											"data" => [
												"password" => md5($password)
											]
										]);
										if ($update_password) {
											$send_otp = OtpControl::__Send_Short_Message(Core::__Body_Request()["mobile"], "New Password for your login to cankul application is " . $password);
											if ($send_otp) {
												Core::__Send_Output([
													"status" => true,
													"message" => "New Password has been sent to your number",
												]);	
											}else{
												Core::__Send_Output([
													"status" => false,
													"message" => "Failed while sending OTP, Phone number might be wrong",
												]);	
											}
										}else{
											Core::__Send_Output([
												"status" => false,
												"message" => "Failed while updating password",
											]);	
										}
									}else{
										// Changing Password
										// Will send OTP Code as verification code
										// Thatll eventually need to verified
										$code = substr(str_shuffle("1234567890QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm@$!"), 0, 6);
										$send_otp = OtpControl::__Send_Short_Message(Core::__Body_Request()["mobile"], "Your OTP Verification code is " . $code);
										if ($send_otp) {
											Core::__Send_Output([
												"status" => true,
												"message" => "OTP Verification Code has been sent to your number",
												"code" => $code
											]);	
										}else{
											Core::__Send_Output([
												"status" => false,
												"message" => "Failed while sending OTP, Phone number might be wrong",
											]);	
										}
									}
								}else{
									Core::__Send_Output([
										"status" => false,
										"message" => "Mobile number is not exist",
									]);		
								}
							}
						}

		public
			static
				function
					_ocr_credit_score(){
						if (Core::__Required_Params(["mobile"])) {
							$read_user_data = RapidDataModel::read('borrowers', [
								"where" => [
									"mobile" => Core::__Body_Request()["mobile"]
								]
							])["rows"];
							if (count($read_user_data) > 0 ) {
								$check_ocr_in_table = RapidDataModel::read("advance_ai_ocr", [
									"where" => [
										"mobile" => Core::__Body_Request()["mobile"],
										"endpoint" => "OCR_CREDIT_SCORE"
									]
								])["rows"];
								// Data already exist
								if (count($check_ocr_in_table) > 0) {
									Core::__Send_Output([
										"status" => true,
										"first_request" => false,
										"data" => json_decode($check_ocr_in_table[0]["raw_json"])
									]);				
								}else{
									// Data ocr credit score
									// Get Previous Data from Check KTP
									$ocr_check_ktp = RapidDataModel::read("advance_ai_ocr", [
										"where" => [
											"mobile" => Core::__Body_Request()["mobile"],
											"endpoint" => "OCR_CHECK_KTP"
										]
									])["rows"];
									if (count($ocr_check_ktp) > 0 ) {
										$ocr_check_ktp_id = json_decode($ocr_check_ktp[0]["raw_json"])->idNumber;
										$ocr_check_ktp_name = json_decode($ocr_check_ktp[0]["raw_json"])->name;
										$ocr_credit_score = AdvanceAIControl::OCR_credit_score(array(
				                            'name' => $ocr_check_ktp_name,
											'idNumber' =>  $ocr_check_ktp_id,
				                            'phoneNumber' => '+' . self::localize_phone_number(Core::__Body_Request()["mobile"])
										));
										$insert_data_ocr_credit = RapidDataModel::insert("advance_ai_ocr", [
											"mobile" => Core::__Body_Request()["mobile"],
											"endpoint" => "OCR_CREDIT_SCORE",
											"raw_json" => json_encode($ocr_credit_score->data)
										]);
										if ($insert_data_ocr_credit) {
											Core::__Send_Output([
												"status" => true,
												"first_request" => true,
												"data" => $ocr_credit_score->data,
												"input" => array(
						                            'name' => $ocr_check_ktp_name,
													'idNumber' =>  $ocr_check_ktp_id,
						                            'phoneNumber' => '+' . self::localize_phone_number(Core::__Body_Request()["mobile"])
												)
											]);	
										}else{
											Core::__Send_Output([
												"status" => false,
												"message" => "Failed while trying to insert data",
											]);	
										}
									}else{
										Core::__Send_Output([
											"status" => false,
											"message" => "You have never had your ID card checked",
										]);	
									}
								}
							}else{
								Core::__Send_Output([
									"status" => false,
									"message" => "Mobile number is not exist",
								]);	
							}
						}
					}

	}

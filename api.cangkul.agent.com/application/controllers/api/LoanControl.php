<?php  
	class LoanControl extends Core{

        protected static $_loan_minimum_amount;
        protected static $_loan_maximum_amount;
		protected static $_loan_default_amount;

		public function __construct(){
			parent::__construct();
		}

		public
			static
				function
					__Test(){
						echo "Test coming from". __CLASS__;
					}

		public
			static
				function 
					_Product(){
                        $select = "FLOOR(((FLOOR(maximum_principal)*maximum_interest_rate*default_loan_duration)+FLOOR(maximum_principal))) as total_payment,id, user_id, name, loan_disbursed_by_id, FLOOR(minimum_principal) as minimum_principal, FLOOR(default_principal) as default_principal, FLOOR(maximum_principal) as maximum_principal , interest_method, interest_rate, interest_period, FLOOR(minimum_interest_rate) as minimum_interest_rate, FLOOR(default_interest_rate) as default_interest_rate, FLOOR(maximum_interest_rate) as maximum_interest_rate, override_interest, FLOOR(override_interest_amount) as override_interest_amount, default_loan_duration, default_loan_duration_type, repayment_cycle, decimal_places, repayment_order, loan_fees_schedule, branch_access, grace_on_interest_charged, advanced_enabled, enable_late_repayment_penalty, enable_after_maturity_date_penalty, after_maturity_date_penalty_type, late_repayment_penalty_type, late_repayment_penalty_calculate, after_maturity_date_penalty_calculate, late_repayment_penalty_amount, after_maturity_date_penalty_amount, late_repayment_penalty_grace_period, after_maturity_date_penalty_grace_period, late_repayment_penalty_recurring, after_maturity_date_penalty_recurring, accounting_rule, after_maturity_date_penalties, after_maturity_date_penalty_system_type";
                        if (empty(Core::__Body_Request()["id"])) {
                            $condition = array(
                                "select" => $select
                            );
                            $loan_product = RapidDataModel::read('loan_products', $condition)["rows"];
                            Core::__Send_Output([
                                "status" => true,
                                "data" => $loan_product
                            ]);
                        }else{
                             $condition = array(
                                "select" => $select,
                                "where" => ["id" => Core::__Body_Request()["id"]]
                             );
                            $loan_product = RapidDataModel::read('loan_products', $condition)["rows"];
                            Core::__Send_Output([
                                "status" => true,
                                "data" => $loan_product[0]
                            ]);
                        }

                    }

        private 
            static
                function
                    __Validate_Amount($amount){
                        // If amount user entered less than minimum amount of product
                        if ($amount < self::$_loan_minimum_amount) {
                            // Return false
                            return false;
                        }else{
                            // If amount user entered more than maximum amount of product
                            if ($amount > self::$_loan_minimum_amount) {
                                // return false
                                return false;
                            }else{
                                // Amount is acceptable
                                return true;
                            }
                        }
                    }

        private
            static 
                function
                    __Create_Aplly_Data(){
                        $create_data = RapidDataModel::create("loan_applications", [
                            [
                                "borrower_id" => Core::__Body_Request()["borrower_id"],
                                "loan_product_id" => Core::__Body_Request()["loan_product_id"],
                                "amount" => Core::__Body_Request()["amount"],
                                "status" => Core::__Body_Request()["status"] ?? "pending",
                                "notes" => Core::__Body_Request()["notes"] ?? "",
                                "created_at" =>  date("Y:m:d h:i:s"),
                                "updated_at" => date("Y:m:d h:i:s"),
                                "branch_id" => Core::__Body_Request()["branch_id"] ?? 1,
                            ]
                        ]);
                        if (!empty($create_data)) {
                            Core::__Send_Output([
                                "status" => true,
                                "data" => [
                                    "id" => $create_data
                                ],
                                "message" => "Loan succesfully applied, Now waiting for approval"
                            ]);
                        }else{
                            Core::__Send_Output([
                                "status" => false,
                                "message" => API_FAILED_INSERT_DATABASE,
                                "message_error" => "Failed inserting data, please try again."
                            ]);
                        }
                    }

        private
            static 
                function
                    __Create_Aplly_Data_OCR($args){
                        $user_data = RapidDataModel::read("borrowers", [
                            "where" => [
                                "mobile" => Core::__Body_Request()["mobile"]
                            ]
                        ])["rows"];
                        if ($user_data[0]["blacklisted"] == 1) {
                            Core::__Send_Output([
                                "status" => false,
                                "message" => "Your account has been blacklisted, Please contact our service if you feel this not right",
                            ]);
                        }else{
                            $create_data = RapidDataModel::create("loan_applications", [
                                [
                                    "borrower_id" => $user_data[0]["id"],
                                    "loan_product_id" => Core::__Body_Request()["loan_product_id"],
                                    "amount" => $args["amount"],
                                    "status" => "pending",
                                    "notes" => Core::__Body_Request()["notes"] ?? "",
                                    "created_at" =>  date("Y:m:d h:i:s"),
                                    "updated_at" => date("Y:m:d h:i:s"),
                                    "branch_id" => Core::__Body_Request()["branch_id"] ?? 1,
                                ]
                            ]);
                            if (!empty($create_data)) {
                                Core::__Send_Output([
                                    "status" => true,
                                    "message" => "Loan succesfully applied, Now waiting for approval",
                                    "data" => [
                                        "id" => $create_data
                                    ]
                                ]);
                            }else{
                                Core::__Send_Output([
                                    "status" => false,
                                    "message" => API_FAILED_INSERT_DATABASE,
                                    "message_error" => "Failed inserting data, please try again."
                                ]);
                            }
                        }
                    }
     
        public
            static
                function
                    _Apply_Ocr(){
                        if (Core::__Required_Params(["loan_product_id", "mobile"])) {
                           if (!RapidDataModel::is_exist('loan_products', ["id" => Core::__Body_Request()["loan_product_id"]])) {
                                Core::__Send_Output([
                                    "status" => false,
                                    "message" => API_FAILED_404_DATA,
                                ]);
                            }else{
                                // Product Score Ranges
                                $get_user_score = RapidDataModel::read("advance_ai_ocr", [
                                    "where" => [
                                        "mobile" => Core::__Body_Request()["mobile"],
                                        "endpoint" => "OCR_CREDIT_SCORE"
                                    ]
                                ])["rows"];
                                $user_score = json_decode($get_user_score[0]["raw_json"])->score;
                                $user_selected_product = Core::__Body_Request()["loan_product_id"];
                                $product_range = RapidDataModel::read("loan_product_range_mapping", [])["rows"];
                                $accepted_products = [];
                                foreach ($product_range as $key => $value) {
                                    if (in_array($user_score, range($value["min_range"], $value["max_range"]))) {
                                        $accepted_products [] = $value["loan_product_id"];
                                    }else{
                                        $other_min_range = $value["min_range"];
                                        $other_max_range = $value["max_range"];
                                        if ($user_score >= $other_min_range) {
                                            $accepted_products [] = $value["loan_product_id"];
                                        }
                                    }
                                }
                                if (in_array($user_selected_product, $accepted_products)) {
                                    // Accepted Application 
                                    $product_detail = RapidDataModel::read("loan_products", [
                                        "select" => "name, maximum_principal as amount, interest_method",
                                        "where" => [
                                            "id" => Core::__Body_Request()["loan_product_id"]
                                        ]
                                    ])["rows"][0];
                                    // Create Apply Data
                                    self::__Create_Aplly_Data_OCR([
                                        "amount" => $product_detail["amount"]
                                    ]);
                                }else{
                                    if (!empty($accepted_products)) {
                                        $product_offers = [];
                                        foreach ($accepted_products as $value) {
                                            $product_detail = RapidDataModel::read("loan_products", [
                                                "select" => "id, name, maximum_principal as amount, interest_method",
                                                "where" => [
                                                    "id" =>  $value
                                                ]
                                            ])["rows"];
                                            $product_offers [] = $product_detail[0];
                                        }
                                        Core::__Send_Output([
                                            "status" => true,
                                            "accepted" => false,
                                            "message" => "Your application does not meet our requirements, however me have something you might want to think about",
                                            "data" => $product_offers
                                        ]);
                                    }else{
                                        Core::__Send_Output([
                                            "status" => false,
                                            "accepted" => false,
                                            "message" => "Your application does not meet our requirements",
                                        ]);
                                    }
                                }
                            }   
                        }
                    }

        public 
            static
                function
                    _Apply(){
                        if (Core::__Required_Params(["loan_product_id", "amount", "borrower_id"])) {
                            if (!RapidDataModel::is_exist('loan_products', ["id" => Core::__Body_Request()["loan_product_id"]])) {
                                Core::__Send_Output([
                                    "status" => false,
                                    "message" => API_FAILED_404_DATA,
                                ]);
                            }else{
                                $select = "*";
                                $condition = array(
                                        "select" => $select,
                                        "where" => ["id" => Core::__Body_Request()["loan_product_id"]]
                                    );
                                $loan_product = RapidDataModel::read('loan_products', $condition)["rows"][0];
                                self::$_loan_minimum_amount = intval($loan_product["minimum_principal"]);
                                self::$_loan_default_amount = intval($loan_product["default_principal"]);
                                self::$_loan_maximum_amount = intval($loan_product["maximum_principal"]);
                                print_r(self::$_loan_minimum_amount);
                                if (self::__Validate_Amount(Core::__Body_Request()["amount"])) {
                                    // Create application data
                                    self::__Create_Aplly_Data();
                                }else{
                                    Core::__Send_Output([
                                         "status" => false,
                                            "message" => API_FAILED_PARAMETER_VALIDATION,
                                            "message_error" => "Amount you entered does not complied to our product's minimum or maximum amount"
                                    ])  ;
                                }
                            }
                        }
                    }
        public
            static
                function 
                    _Myapplication(){

                        $select = "loan_applications.id as id, loan_products.name as loan_product_name, FLOOR(loan_applications.amount) as amount, loan_applications.status as status, loan_applications.notes as notes , loan_applications.created_at as date";
                        
                       

                        if (empty(Core::__Body_Request()["borrower_id"])) {
                            echo "string";
                            $condition = array(
                                "select" => $select
                            );
                            $loan_product = RapidDataModel::read('loan_applications', $condition)["rows"];
                            Core::__Send_Output([
                                "status" => true,
                                "data" => $loan_product
                            ]);
                        }else{
                             $condition = array(
                                "select" => $select,
                                "where" => ["borrower_id" => Core::__Body_Request()["borrower_id"]],
                                "join" => array(
                                        "borrowers"     => "loan_applications.borrower_id = borrowers.id",
                                        "loan_products" => "loan_applications.loan_product_id = loan_products.id"
                                ),
                             );
                            $loan_product = RapidDataModel::read('loan_applications', $condition)["rows"];
                            Core::__Send_Output([
                                "status" => true,
                                "data" => $loan_product

                            ]);
                        } 
                    }
         public
            static
                function 
                    _Riwayatpinjaman(){
                        // echo "string";
                        
            // $select = " 
            //                 FLOOR(loans.principal) as principal,
            //                         FLOOR(loans.override_interest_amount) as override_interest_amount,
            //                         FLOOR(loans.applied_amount) as applied_amount,
            //                         FLOOR(loans.approved_amount) as approved_amount,
            // loans.id,
            // loans.user_id,
            // loans.borrower_id,
            // loans.loan_product_id,
            // loans.reference,
            // loans.release_date,
            // loans.maturity_date,
            // loans.month,
            // loans.year,
            // loans.interest_start_date,
            // loans.first_payment_date,
            // loans.loan_disbursed_by_id,
          
            // loans.interest_method,
            // loans.interest_rate,
            // loans.interest_period,
            // loans.override_interest,
         
            // loan_duration,
            // loans.loan_duration_type,
            // loans.repayment_cycle,
            // loans.decimal_places,
            // loans.repayment_order,
            // loans.loan_fees_schedule,
            // loans.grace_on_interest_charged,
            // loans.loan_status_id,
            // loans.files,
            // loans.description,
            // loans.loan_status,
            // loans.balance,
            // loans.override,
            // loans.created_at,
            // loans.updated_at,
            // loans.deleted_at,
            // loans.status,
            // loans.approved_notes,
            // loans.disbursed_notes,
            // loans.withdrawn_notes,
            // loans.closed_notes,
            // loans.rescheduled_notes,
            // loans.declined_notes,
            // loans.written_off_notes,
            // loans.approved_date,
            // loans.disbursed_date,
            // loans.withdrawn_date,
            // loans.closed_date,
            // loans.rescheduled_date,
            // loans.declined_date,
            // loans.written_off_date,
            // loans.approved_by_id,
            // loans.disbursed_by_id,
            // loans.withdrawn_by_id,
            // loans.declined_by_id,
            // loans.written_off_by_id,
            // loans.rescheduled_by_id,
            // loans.closed_by_id,
            // loans.branch_id,
            // loans.processing_fee,
            // loans.product_check_out_id,
            // loans.loan_officer_id,
            // loan_products.name, 
            //                         ";

                        $select = "loan_products.name,
                                    loans.loan_duration,
                                    loan_products.default_loan_duration_type,
                                    FLOOR(loans.principal) as principal,
                                   loans.first_payment_date,
                                     

                        ";

                        if (empty(Core::__Body_Request()["borrower_id"])) {
                             Core::__Send_Output([
                                "status" => true,
                                // "data" => $loan_product,
                                "keterangan" => "borrower_id kosong"

                            ]);
                        }else{
                             $condition = array(
                                "select" => $select,
                                "where" => ["borrower_id" => Core::__Body_Request()["borrower_id"]],
                                "join" => array(
                                        "borrowers"     => "loans.borrower_id = borrowers.id",
                                        "loan_products" => "loans.loan_product_id = loan_products.id"
                                ),
                             );
                            $loan_product = RapidDataModel::read('loans', $condition)["rows"];
                             if (empty($loan_product)) {
                                Core::__Send_Output([
                                "status" => true,
                                "data" => $loan_product,
                                "keterangan" => "data kosong"

                            ]);
                            }else{
                                Core::__Send_Output([
                                "status" => true,
                                "data" => $loan_product

                            ]);
                            }
                        } 
                }
        public
			static
				function 
					_Repayment(){
                        // echo Core::__Body_Request()["id"];

                
                // lama
                  //       $select = "FLOOR(loan_transactions.credit) as loan_transactions_credit, loan_repayment_methods.name as loan_repayment_method_name

                  // ,borrowers.id as  borrowersid,
                  //  borrowers.first_name as first_name_borrower, 
                  //  borrowers.last_name as last_name_borrower,
                  //  users.first_name as first_name_user,
                  //  users.last_name as last_name_admin,
                  //  loan_transactions.date as collection_date";
                        $select = "loan_transactions.date as date,
                                    loan_repayment_methods.name as name_method,
                                   FLOOR(loan_transactions.credit) as credit,
                                   loan_repayments.due_date as due_date";  
                      
                       

                            $condition = array(
                                "select" => $select,
                                "where" => ["borrowers.id" => Core::__Body_Request()["borrower_id"]],
                                "join" => array(
                                        "loans" => "loans.borrower_id = borrowers.id",
                                        "loan_transactions" => "loans.id = loan_transactions.loan_id",
                                        "branches" => "branches.id = loan_transactions.branch_id",
                                        "users" => "users.id = loan_transactions.user_id",
                                        "loan_repayment_methods" => "loan_repayment_methods.id = loan_transactions.repayment_method_id",
                                        "loan_repayments" => "loan_repayments.borrower_id = borrowers.id",
                                        
                                ),
                             );
                            $loan_product = RapidDataModel::read('borrowers', $condition)["rows"];
                            if (empty($loan_product)) {
                                Core::__Send_Output([
                                "status" => true,
                                "data" => $loan_product,
                                "keterangan" => "data kosong"

                            ]);
                            }else{
                                Core::__Send_Output([
                                "status" => true,
                                "data" => $loan_product

                            ]);
                            }

					  

                    }
	}
  
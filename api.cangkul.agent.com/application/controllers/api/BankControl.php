<?php  

	class BankControl extends Core{
		
        protected static $_user_register_id;
		public function __construct(){
			parent::__construct();
		}

		public
			static
				function
					__Test(){
						echo "Test coming from". __CLASS__;
					}

        public 
            static
                function
                    _List(){
                        // echo "test";
                    
                        $select = "*";  
                      
                       

                            $condition = array(
                                "select" => $select,
                                // "where" => ["borrower_id" => Core::__Body_Request()["borrower_id"]],
                                // "join" => array(
                                //         "borrowers"     => "inbox_message.borrower_id = borrowers.id",
                                //         ),
                               
                                        
                               
                             );
                            $inbox_message = RapidDataModel::read('bank_accounts', $condition)["rows"];
                            if (empty($inbox_message)) {
                                Core::__Send_Output([
                                "status" => true,
                                "data" => $inbox_message,
                                "message" => "data kosong"

                            ]);
                            }else{
                                Core::__Send_Output([
                                "status" => true,
                                "data" => $inbox_message

                            ]);
                            }

                      
                    }
        public 
            static
                function
                    _Getlist(){
                        
                        $select = "*";  
                      
                       

                            $condition = array(
                                "select" => $select,
                                "where" => ["borrowers.id" => Core::__Body_Request()["borrower_id"]],
                                "join" => array(
                                        "borrowers"     => "borrower_account.borrower_id = borrowers.id",
                                        "bank_accounts"     => "bank_accounts.id = borrower_account.bank_id",
                                        ),
                               
                                        
                               
                             );
                            $inbox_message = RapidDataModel::read('borrower_account', $condition)["rows"];
                            if (empty($inbox_message)) {
                                Core::__Send_Output([
                                "status" => true,
                                "data" => $inbox_message,
                                "message" => "data kosong bank pada id borrower : ".Core::__Body_Request()["borrower_id"],

                            ]);
                            }else{
                                Core::__Send_Output([
                                "status" => true,
                                "data" => $inbox_message

                            ]);
                            }

                      
                    }
       public 
            static
                function
                    _Createdata(){
                        // echo "string";

                        if (empty(Core::__Body_Request()["bank_id"])) {
                            Core::__Send_Output([
                                "status" => true,
                                "message" => "bank_id empty"

                            ]);
                        }
                        if (empty(Core::__Body_Request()["no_rek"])) {
                            Core::__Send_Output([
                                "status" => true,
                                "message" => "no_rek empty"

                            ]);
                        }
                         if (empty(Core::__Body_Request()["borrower_id"])) {
                            Core::__Send_Output([
                                "status" => true,
                                "message" => "borrower_id empty"

                            ]);
                        }



                      
                        $create_data = RapidDataModel::create("borrower_account", [
                            [
                                "bank_id" => Core::__Body_Request()["bank_id"] ?? "",
                                "no_rek" => Core::__Body_Request()["no_rek"] ?? "",
                                "borrower_id" => Core::__Body_Request()["borrower_id"] ?? "",
                                
                               
                            ]
                        ]);

                        if (!empty($create_data)) {
                            Core::__Send_Output([
                                "status" => true,
                                "message" => "Data berhasil di simpan"

                            ]);
                        }
                       
                    }
        public
                static
                    function
                        _Update(){
                            if (Core::__Required_Params(array("borrower_id", "data"))) {
                                RapidDataModel::table('borrower_account');
                                $available_fields = RapidDataModel::read_fields();
                                $data_to_update = [];
                                $NOT_updateable_data = ["id", "borrower_id"];
                                $data_not_allowed_to_update = [];
                                // Loop through body data request
                                foreach (Core::__Body_Request()["data"] as $key => $value) {
                                    // If data key is in acceptable fields
                                    if (in_array($key, $available_fields)) {
                                        if (in_array($key, $NOT_updateable_data)) {
                                            $data_not_allowed_to_update[] = $key;
                                        }else{
                                            // Store data to variable
                                            $data_to_update[$key] = $value;
                                        }
                                    }else{
                                        // Store data to not updatable variable
                                        $data_not_allowed_to_update[] = $key;
                                    }
                                }
                                // If not allowed to update data parameter is empty
                                if (empty($data_not_allowed_to_update)) {
                                    // And if the data to update ! empty
                                    if (!empty($data_to_update)) {
                                        // Proceed update data
                                        $update = RapidDataModel::update('borrower_account', [
                                            "key" => ["borrower_id" => Core::__Body_Request()["borrower_id"]],
                                            "data" => $data_to_update
                                        ]);
                                        if ($update) {
                                            Core::__Send_Output([
                                                "status" => true,
                                                "message" => "Data succesfully updated",
                                                "data" => $data_to_update
                                            ]);
                                        }else{
                                            Core::__Send_Output([
                                                "status" => false,
                                                "message" => API_FAILED_INSERT_DATABASE,
                                            ]);
                                        }
                                    }else{
                                        // Return response parameter data should not be empty
                                        Core::__Send_Output([
                                            "status" => false,
                                            "message" => API_FAILED_PARAMETER,
                                            "message_error" => "Parameter data should not be empty",
                                        ]);
                                    }
                                }else{
                                    // Return response data is not allowed to update
                                    Core::__Send_Output([
                                        "status" => false,
                                        "message" => API_FAILED_PARAMETER,
                                        "message_error" => "Parameter (" . implode(", ", $data_not_allowed_to_update) . ") is not updateable",
                                    ]);     
                                }
                            }
                        }
       
       
		
       
	}
  
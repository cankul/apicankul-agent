<?php  
	class FileControl extends Core{

		private static $minimum_score_for_photo_with_ktp = 75;

		public function __construct(){
			parent::__construct();
		}

		public
			static
				function
					__Test(){
						echo "Test coming from". __CLASS__;
					}

		public 
			static
				function
					_upload(){
						if (Core::__Required_params(["filename", "file"])) {
							$put_image = file_put_contents(
						        FCPATH . "storage/images/". Core::__Body_Request()["filename"],
						        base64_decode(Core::__Body_Request()["file"])
						    );
						    if ($put_image) {
						    	Core::__Send_Output([
						    		"status" => true,
						    		"message" => "Image sucessfully uploaded to /storage/images/"
						    	]);
						    }else{
						    	Core::__Send_Output([
						    		"status" => false,
						    		"message" => "Failed while creating image"
						    	]);
						    }
						}
					}

		public 
			static
				function
					_upload_form(){
						if (!empty($_FILES)) {
							$image_ext = pathinfo($_FILES["image"]["name"], PATHINFO_EXTENSION);
							$image_name = sha1(uniqid()) . "." . $image_ext;
							$upload = move_uploaded_file($_FILES["image"]["tmp_name"], FCPATH . "storage/images/" . $image_name);
							if ($upload) {
								Core::__Send_Output([
						    		"status" => true,
						    		"message" => "File uploaded",
						    		"filename" =>  $image_name
						    	]);
							}else{
								Core::__Send_Output([
						    		"status" => false,
						    		"message" => "Failed uploading file, make sure extension is correct etc."
						    	]);
							}
						}else{
							Core::__Send_Output([
						    	"status" => false,
						    	"message" => "There is no file to upload"
						    ]);
						}
					}
		public
			static
				function
					_read_stored_advance_ai($mobile, $field){
						$read = RapidDataModel::read("advance_ai", [
							"where" => [
								"mobile" => $mobile
							]
						])["rows"];
						if (count($read) > 0) {
							Core::__Send_Output([
							  	"status" => true,
								"first_request" => false,
							  	"data" => json_decode($read[0][$field])
							]);
						}else{
							// phone not exist in table
							self::_store_advance_ai();
						}
					}

		public
			static
				function
					_store_advance_ai($image_name){
						$check_ocr = AdvanceAIControl::OCR_ktp_check($image_name);
						if ($check_ocr->code == "SUCCESS") {
							$insert_ocr_ktp_check_repsonse = RapidDataModel::insert("advance_ai", [
								"mobile" => $_POST["mobile"],
								"ocr_ktp_check" => json_encode($check_ocr->data),
							]);
							$update_user_data = RapidDataModel::update('borrowers', [
								"key" => [
											"mobile" => $_POST["mobile"]
								],
								"data" => [
									"photo_id_card" => $image_name,
									"first_name" => $check_ocr->data->name,	
									"city" => $check_ocr->data->name,	
									"address" =>  $check_ocr->data->address . " ".  $check_ocr->data->village . " " . $check_ocr->data->rtrw,
									"province" => $check_ocr->data->province
								]
							]);
							if ($update_user_data) {
								Core::__Send_Output([
									"status" => true,
									"data" => $check_ocr->data
								]);
							}else{
								Core::__Send_Output([
									"status" => false,
									"message" => "Failed while updating user data"
								]);
							}
						}else{
							Core::__Send_Output([
								"status" => false,
								"message" => $check_ocr->message
							]);
						}
					}

			private
				static
					function
						_upload_file($temp, $name){
							$image_ext = pathinfo($_FILES["image"]["name"], PATHINFO_EXTENSION);
							$image_name = sha1(uniqid()) . "." . $image_ext;
							$upload = move_uploaded_file($temp, FCPATH . "storage/images/" . $image_name);
						}

			public
				static
					function
						_upload_ocr_check_ktp(){
							if (isset($_POST["mobile"])) {
								// $check_data_in_advance_ai = RapidDataModel::read("advance_ai_ocr", [
								// 	"where" => [
								// 		"mobile" => $_POST["mobile"],
								// 		"endpoint" => "OCR_CHECK_KTP"
								// 	]
								// ])["rows"];
								// Data not Exist in table 
								// Upload new file
								// if (count($check_data_in_advance_ai) == 0) {
									if (!empty($_FILES)) {
										// Upload new photo KTP
										$image_ext = pathinfo($_FILES["image"]["name"], PATHINFO_EXTENSION);
										$image_name = sha1(uniqid()) . "." . $image_ext;
										$upload = move_uploaded_file($_FILES["image"]["tmp_name"], FCPATH . "storage/images/" . $image_name);
										// User not exist in table
										$check_ocr = AdvanceAIControl::OCR_ktp_check($image_name);
										// IF Advance AI Response == successs
										if ($check_ocr->code == "SUCCESS") {
											// Insert a new colum to advance ai table
											$update_user_data = RapidDataModel::update('borrowers', [
												"key" => ["mobile" => $_POST["mobile"]],
												"data" => [
													"photo_id_card" => $image_name,
													"first_name" => $check_ocr->data->name,	
													"city" => $check_ocr->data->name,	
													"address" =>  $check_ocr->data->address . " ".  $check_ocr->data->village . " " . $check_ocr->data->rtrw,
													"province" => $check_ocr->data->province
												]
											]);
											if ($update_user_data) {
												Core::__Send_Output([
													"status" => true,
													"data" => $check_ocr->data
												]);
											}else{
												Core::__Send_Output([
													"status" => false,
													"message" => "Failed while updating user data"
												]);
											}
										}else{
											Core::__Send_Output([
												"status" => false,
												"message" => $check_ocr->message
											]);
										}
									}else{
										Core::__Send_Output([
											"status" => false,
											"message" => "Failed, No file to be upload"
										]);
									}
								// }else{
								// 	// Data Exist
								// 	// Only check existing OCR
								// 	$read = RapidDataModel::read("advance_ai_ocr", [
								// 		"where" => [
								// 			"mobile" => $_POST["mobile"],
								// 			"endpoint" => "OCR_CHECK_KTP"
								// 		]
								// 	])["rows"];
								// 	if (count($read) > 0) {
								// 		Core::__Send_Output([
								// 			"status" => true,
								// 			"first_request" => false,
								// 			"data" => json_decode($read[0]["raw_json"])
								// 		]);
								// 	}else{
								// 		Core::__Send_Output([
								// 			"status" => false,
								// 			"message" => "Data OCR not found"
								// 		]);
								// 	}
								// }	
							}else{
								Core::__Send_Output([
									"status" => false,
									"message" => "Failed, Parameter mobile is required"
								]);
							}
						}

			public
				static
					function
						_upload_ocr_verify_photo_with_ktp(){
							if (isset($_POST["mobile"])) {
								// $check_data_in_advance_ai = RapidDataModel::read("advance_ai_ocr", [
								// 	"where" => [
								// 		"mobile" => $_POST["mobile"],
								// 		"endpoint" => "OCR_VERIFY_PHOTO_WITH_ID"
								// 	]
								// ])["rows"];
								// Data not Exist in table 
								// Upload new file
								// if (count($check_data_in_advance_ai) == 0) {
									if (!empty($_FILES)) {
										// Upload new photo KTP
										$image_ext = pathinfo($_FILES["image"]["name"], PATHINFO_EXTENSION);
										$image_name = sha1(uniqid()) . "." . $image_ext;
										$upload = move_uploaded_file($_FILES["image"]["tmp_name"], FCPATH . "storage/images/" . $image_name);
										// User not exist in table
										$check_ocr = AdvanceAIControl::Verify_IDCard($image_name);
										// IF Advance AI Response == successs
										if ($check_ocr->code == "SUCCESS") {
											// Insert a new colum to advance ai table
											if ($check_ocr->data->similarity >= self::$minimum_score_for_photo_with_ktp) {
												// $insert_ocr_ktp_check_repsonse = RapidDataModel::insert("advance_ai_ocr", [
												// 	"endpoint" => "OCR_VERIFY_PHOTO_WITH_ID",
												// 	"raw_json" => json_encode($check_ocr->data),
												// 	"mobile" => $_POST["mobile"]
												// ]);
												Core::__Send_Output([
													"status" => true,
													"score" => $check_ocr->data->similarity
												]);
											}else{
												Core::__Send_Output([
													"status" => false,
													"message" => "Failed identifying your ID",
													"score" => $check_ocr->data->similarity
												]);
											}
										}else{
											Core::__Send_Output([
												"status" => false,
												"message" => $check_ocr->message
											]);
										}
									}else{
										Core::__Send_Output([
											"status" => false,
											"message" => "Failed, No file to be upload"
										]);
									}
								// }else{
								// 	// Data Exist
								// 	// Only check existing OCR
								// 	$read = RapidDataModel::read("advance_ai_ocr", [
								// 		"where" => [
								// 			"mobile" => $_POST["mobile"],
								// 			"endpoint" => "OCR_VERIFY_PHOTO_WITH_ID"
								// 		],
								// 	])["rows"];
								// 	if (count($read) > 0) {
								// 		$ocr_photo_with_id = json_decode($read[0]["raw_json"]);
								// 		if ($ocr_photo_with_id->similarity >= self::$minimum_score_for_photo_with_ktp) {
								// 			Core::__Send_Output([
								// 				"status" => true,
								// 				"first_request" => false,
								// 				"score" => $ocr_photo_with_id->similarity
								// 			]);
								// 		}else{
								// 			Core::__Send_Output([
								// 				"status" => false,
								// 				"first_request" => false,
								// 				"message" => "Failed identifying your ID"
								// 			]);
								// 		}
								// 	}else{
								// 		Core::__Send_Output([
								// 			"status" => false,
								// 			"message" => "You have never had your Photo With ID card uploaded",
								// 		]);	
								// 	}
								// }	
							}else{
								Core::__Send_Output([
									"status" => false,
									"message" => "Failed, Parameter mobile is required"
								]);
							}
						}

	}

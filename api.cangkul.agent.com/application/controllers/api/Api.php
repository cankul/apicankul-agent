<?php  
	require 'Core.php';
	require 'UserControl.php';
	require 'LoanControl.php';
	require 'LoanreportControl.php';
	require 'OtpControl.php';
	require 'FileControl.php';
	require 'RepaymentControl.php';
	require 'GeneralControl.php';
	require 'AdvanceAiControl.php';
	require 'InboxControl.php';
	require 'BankControl.php';
 

	class Api extends Core{
		
		public function __construct(){
			parent::__construct();
		}

		public
			static
				function 
					__Defined_Classes(){
						return array(
							"user" => "UserControl",
							"product" => "ProductControl",
							"loan" => "LoanControl",
							"loanreport" => "LoanreportControl",
							"otp" => "OtpControl",
							"file" => "FileControl",
							"repayment" => "RepaymentControl",
							"general" => "GeneralControl",
							"advai" => "AdvanceAiControl",
							"inbox" => "InboxControl",
							"bank" => "BankControl"
 						);
					}

		// Main function
		public 
			function 
				init_POST(){
					$req_url = $this->uri->segment_array();
					if (Core::__Check_Token()) {
						$user_token_verification_exceptions = ["_login", "_register", "_send", "_verify", "_register_password", "_upload_ocr_check_ktp", "_upload_ocr_verify_photo_with_ktp", "_ocr_credit_score", "_upload_form_verify_idcard", "_upload_form_check_ocr_ktp", "_forgot_password", "_upload_form"];
						$function = implode("_", $req_url);
						$function = str_replace($req_url[1], "", $function);
						// endpoint who doesnt need to 	
						if (method_exists(self::__Defined_Classes()[$req_url[1]], $function)){
							if (in_array($function, $user_token_verification_exceptions)) {
								self::__Defined_Classes()[$req_url[1]]::$function();
							}else{
								// If its not login endpoint
								// Check for user token
								if (Core::__Check_User_Token()) {
									self::__Defined_Classes()[$req_url[1]]::$function();
								}else{
									Core::__Send_Output(array(
										"status" => false,
										"error_code" => 400,
										"message" => API_FAILED_CONNECT,
										"message_error" => Core::__Get_Errors()
									));
								}
							}
						}else{
							Core::__Send_Output(array(
								"status" => false,
								"error_code" => 400,
								"message" => API_FAILED_CONNECT,
								"message_error" => "Method you trying to access is not exist",
							));
						}
					}else{
						Core::__Send_Output(array(
							"status" => false,
							"error_code" => 400,
							"message" => API_FAILED_CONNECT,
							"message_error" => Core::__Get_Errors()
						));
					}
				}
	}

<?php  
    include_once APPPATH . 'libraries/advanceAI/advance/CurlClient.php';

	class AdvanceAiControl extends Core{
		protected static $access_key = '07bd6ffccbbb18e1'; // your access key
        protected static $secret_key = '258742464b95b5de'; // your secret key

		public function __construct(){
			parent::__construct();
		}

		public
			static
				function
					__Test(){
						echo "Test coming from". __CLASS__;
					}

        public
            static
                function
                    OCR_ktp_check($image){
                        $api_host = 'https://api.advance.ai';
                        $api_name = '/openapi/face-recognition/v1/ocr-ktp-check';
                        $access_key = self::$access_key;
                        $secret_key = self::$secret_key;

                        $ocrImage = array(
                            'ocrImage' =>  FCPATH . "storage/images/". $image
                        );

                        $client = new ai\advance\CurlClient($api_host, $access_key, $secret_key);

                        $result = $client -> request($api_name, null, $ocrImage);
                        return json_decode($result);
                    }

        public
            static
                function
                    Verify_IDCard($image){
            
                        $api_host = 'https://api.advance.ai';
                        $api_name = '/openapi/face-recognition/v3/id-check';
                        $access_key = self::$access_key;
                        $secret_key = self::$secret_key;
                        $params = array(
                            'idHoldingImage' => FCPATH . "storage/images/". $image
                        );
                        $client = new ai\advance\CurlClient($api_host, $access_key, $secret_key);

                        $result = $client->request($api_name, null, $params);
                        return json_decode($result);
                    }
        public
            static
                function
                    OCR_credit_score($params){
                        $api_host = 'https://api.advance.ai';
                        $api_name = '/openapi/score/v1/credit';
                        $access_key = self::$access_key;
                        $secret_key = self::$secret_key;

                        $client = new ai\advance\CurlClient($api_host, $access_key, $secret_key);

                        $result = $client -> request($api_name, $params);
                        return json_decode($result);
                    }
	}
  
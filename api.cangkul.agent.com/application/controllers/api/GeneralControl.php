<?php  
	class GeneralControl extends Core{
		
		public function __construct(){
			parent::__construct();
		}
       
		public
			static
				function
					__Test(){
						echo "Test coming from". __CLASS__;
					}

		
        public
            static
                function 
                    _Borrower_loans_total_paid(){
                       $paid = 0;
                         $condition1 = array(
                                "select" => "*",
                                "where" => ["borrower_id" => Core::__Body_Request()["borrower_id"]],
                               
                             );
                        $loan = RapidDataModel::read('loans', $condition1)["rows"];

                        foreach ($loan as $key ) {
                            // print_r($key['id']);
                            $condition = array(
                                "select" => "FLOOR(SUM(credit)) as credit",
                                "where" => ["loan_id" => $key['id'] , "transaction_type" => "repayment", "reversed" => 0],
                               
                             );
                        $paid = $paid + RapidDataModel::read('loan_transactions', $condition)["rows"];
                        }
                        print_r($paid);
                        Core::__Send_Output([
                                "status" => true,
                                "data" => $paid,
                                "keterangan" => "_Borrower_loans_total_paid"

                        ]);
                       
                      

                    }
        public
            static
                function 
                    _Loanpreode()
                      {
                        $condition1 = array(
                                "select" => "*",
                                "where" => ["borrower_id" => Core::__Body_Request()["id"]],
                               
                             );
                        $loan =  (object)RapidDataModel::read('loans', $condition1)["rows"][0];
                        
                        $period = 0;
                        if ($loan->repayment_cycle == 'annually') {
                            if ($loan->loan_duration_type == 'year') {
                                $period = ceil($loan->loan_duration);
                            }
                            if ($loan->loan_duration_type == 'month') {
                                $period = ceil($loan->loan_duration * 12);
                            }
                            if ($loan->loan_duration_type == 'week') {
                                $period = ceil($loan->loan_duration * 52);
                            }
                            if ($loan->loan_duration_type == 'day') {
                                $period = ceil($loan->loan_duration * 365);
                            }
                        }
                        if ($loan->repayment_cycle == 'semi_annually') {
                            if ($loan->loan_duration_type == 'year') {
                                $period = ceil($loan->loan_duration * 2);
                            }
                            if ($loan->loan_duration_type == 'month') {
                                $period = ceil($loan->loan_duration * 6);
                            }
                            if ($loan->loan_duration_type == 'week') {
                                $period = ceil($loan->loan_duration * 26);
                            }
                            if ($loan->loan_duration_type == 'day') {
                                $period = ceil($loan->loan_duration * 182.5);
                            }
                        }
                        if ($loan->repayment_cycle == 'quarterly') {
                            if ($loan->loan_duration_type == 'year') {
                                $period = ceil($loan->loan_duration);
                            }
                            if ($loan->loan_duration_type == 'month') {
                                $period = ceil($loan->loan_duration * 12);
                            }
                            if ($loan->loan_duration_type == 'week') {
                                $period = ceil($loan->loan_duration * 52);
                            }
                            if ($loan->loan_duration_type == 'day') {
                                $period = ceil($loan->loan_duration * 365);
                            }
                        }
                        if ($loan->repayment_cycle == 'bi_monthly') {
                            if ($loan->loan_duration_type == 'year') {
                                $period = ceil($loan->loan_duration * 6);
                            }
                            if ($loan->loan_duration_type == 'month') {
                                $period = ceil($loan->loan_duration / 2);

                            }
                            if ($loan->loan_duration_type == 'week') {
                                $period = ceil($loan->loan_duration * 8);
                            }
                            if ($loan->loan_duration_type == 'day') {
                                $period = ceil($loan->loan_duration * 60);
                            }
                        }

                        if ($loan->repayment_cycle == 'monthly') {
                            if ($loan->loan_duration_type == 'year') {
                                $period = ceil($loan->loan_duration * 12);
                            }
                            if ($loan->loan_duration_type == 'month') {
                                $period = ceil($loan->loan_duration);
                            }
                            if ($loan->loan_duration_type == 'week') {
                                $period = ceil($loan->loan_duration * 4.3);
                            }
                            if ($loan->loan_duration_type == 'day') {
                                $period = ceil($loan->loan_duration * 30.4);
                            }
                        }
                        if ($loan->repayment_cycle == 'weekly') {
                            if ($loan->loan_duration_type == 'year') {
                                $period = ceil($loan->loan_duration * 52);
                            }
                            if ($loan->loan_duration_type == 'month') {
                                $period = ceil($loan->loan_duration * 4);
                            }
                            if ($loan->loan_duration_type == 'week') {
                                $period = ceil($loan->loan_duration * 1);
                            }
                            if ($loan->loan_duration_type == 'day') {
                                $period = ceil($loan->loan_duration * 7);
                            }
                        }
                        if ($loan->repayment_cycle == 'daily') {
                            if ($loan->loan_duration_type == 'year') {
                                $period = ceil($loan->loan_duration * 365);
                            }
                            if ($loan->loan_duration_type == 'month') {
                                $period = ceil($loan->loan_duration * 30.42);
                            }
                            if ($loan->loan_duration_type == 'week') {
                                $period = ceil($loan->loan_duration * 7.02);
                            }
                            if ($loan->loan_duration_type == 'day') {
                                $period = ceil($loan->loan_duration);
                            }
                        }
                        return $period;

                    }

        public
            static
                function 
                    _Determine_interest_rate()
                    {
                        $condition1 = array(
                                "select" => "*",
                                "where" => ["borrower_id" => Core::__Body_Request()["id"]],

                               
                             );
                        $loan =  (object)RapidDataModel::read('loans', $condition1)["rows"][0];
                      
                        // $loan = Loan::find($id);
                        $interest = '';
                        if ($loan->override_interest == 1) {
                            $interest = $loan->override_interest_amount;
                        } else {
                            if ($loan->repayment_cycle == 'annually') {
                                //return the interest per year
                                if ($loan->interest_period == 'year') {
                                    $interest = $loan->interest_rate;
                                }
                                if ($loan->interest_period == 'month') {
                                    $interest = $loan->interest_rate * 12;
                                }
                                if ($loan->interest_period == 'week') {
                                    $interest = $loan->interest_rate * 52;
                                }
                                if ($loan->interest_period == 'day') {
                                    $interest = $loan->interest_rate * 365;
                                }
                            }
                            if ($loan->repayment_cycle == 'semi_annually') {
                                //return the interest per semi annually
                                if ($loan->interest_period == 'year') {
                                    $interest = $loan->interest_rate / 2;
                                }
                                if ($loan->interest_period == 'month') {
                                    $interest = $loan->interest_rate * 6;
                                }
                                if ($loan->interest_period == 'week') {
                                    $interest = $loan->interest_rate * 26;
                                }
                                if ($loan->interest_period == 'day') {
                                    $interest = $loan->interest_rate * 182.5;
                                }
                            }
                            if ($loan->repayment_cycle == 'quarterly') {
                                //return the interest per quaterly

                                if ($loan->interest_period == 'year') {
                                    $interest = $loan->interest_rate / 4;
                                }
                                if ($loan->interest_period == 'month') {
                                    $interest = $loan->interest_rate * 3;
                                }
                                if ($loan->interest_period == 'week') {
                                    $interest = $loan->interest_rate * 13;
                                }
                                if ($loan->interest_period == 'day') {
                                    $interest = $loan->interest_rate * 91.25;
                                }
                            }
                            if ($loan->repayment_cycle == 'bi_monthly') {
                                //return the interest per bi-monthly
                                if ($loan->interest_period == 'year') {
                                    $interest = $loan->interest_rate / 6;
                                }
                                if ($loan->interest_period == 'month') {
                                    $interest = $loan->interest_rate * 2;
                                }
                                if ($loan->interest_period == 'week') {
                                    $interest = $loan->interest_rate * 8.67;
                                }
                                if ($loan->interest_period == 'day') {
                                    $interest = $loan->interest_rate * 58.67;
                                }

                            }

                            if ($loan->repayment_cycle == 'monthly') {
                                //return the interest per monthly

                                if ($loan->interest_period == 'year') {
                                    $interest = $loan->interest_rate / 12;
                                }
                                if ($loan->interest_period == 'month') {
                                    $interest = $loan->interest_rate * 1;
                                }
                                if ($loan->interest_period == 'week') {
                                    $interest = $loan->interest_rate * 4.33;
                                }
                                if ($loan->interest_period == 'day') {
                                    $interest = $loan->interest_rate * 30.4;
                                }
                            }
                            if ($loan->repayment_cycle == 'weekly') {
                                //return the interest per weekly

                                if ($loan->interest_period == 'year') {
                                    $interest = $loan->interest_rate / 52;
                                }
                                if ($loan->interest_period == 'month') {
                                    $interest = $loan->interest_rate / 4;
                                }
                                if ($loan->interest_period == 'week') {
                                    $interest = $loan->interest_rate;
                                }
                                if ($loan->interest_period == 'day') {
                                    $interest = $loan->interest_rate * 7;
                                }
                            }
                            if ($loan->repayment_cycle == 'daily') {
                                //return the interest per day

                                if ($loan->interest_period == 'year') {
                                    $interest = $loan->interest_rate / 365;
                                }
                                if ($loan->interest_period == 'month') {
                                    $interest = $loan->interest_rate / 30.4;
                                }
                                if ($loan->interest_period == 'week') {
                                    $interest = $loan->interest_rate / 7.02;
                                }
                                if ($loan->interest_period == 'day') {
                                    $interest = $loan->interest_rate * 1;
                                }
                            }
                        }
                        return $interest / 100;
                        // print_r($interest/100);
                    }
        public
            static
                function 
                    _Borrower_loans_total_due(){
                       $due = 0;
                         $condition1 = array(
                                "select" => "*",
                                "where" => ["borrower_id" => Core::__Body_Request()["borrower_id"]],
                               
                             );
                        $loan = RapidDataModel::read('loans', $condition1)["rows"];

                        foreach ($loan as $key ) {
                            
                          $due =$due + GeneralControl::_Loan_total_due_amount($key['id']);
                        }
                        Core::__Send_Output([
                                "status" => true,
                                "data" => $due,
                                "keterangan" => "_Borrower_loans_total_due"

                        ]);
                       


                       // $due = 0;
                       //  foreach (Loan::whereIn('status',
                       //      ['disbursed', 'closed', 'written_off'])->where('borrower_id', $id)->get() as $key) {
                       //      $due = $due + GeneralHelper::loan_total_due_amount($key->id);
                       //  }
                       //  return $due;
                      

                    }

//========================penambahan===================
        public static function _Loan_total_due_amount($id="", $due_date = "")
            {
                if (empty($id)) {
                    return (GeneralControl::_Loan_total_penalty($id) + GeneralControl::_Loan_total_fees($id) + GeneralControl::_Loan_total_interest($id) + GeneralControl::_Loan_total_principal($id) - GeneralControl::_Loan_total_interest_waived($id));
                } else {
                    return (GeneralControl::_Loan_total_penalty($id, $due_date) + GeneralControl::_Loan_total_fees($id,
                            $due_date) + GeneralControl::_Loan_total_interest($id, $due_date) + GeneralControl::_Loan_total_principal($id,
                            $due_date) - GeneralControl::_Loan_total_interest_waived($id, $due_date));

                }
            

            }
            public static function _Loan_total_due_amount_get()
            {
                if (empty(Core::__Body_Request()["due_date"])) {
                    return (GeneralControl::_Loan_total_penalty(Core::__Body_Request()["loan_id"]) + GeneralControl::_Loan_total_fees(Core::__Body_Request()["loan_id"]) + GeneralControl::_Loan_total_interest(Core::__Body_Request()["loan_id"]) + GeneralControl::_Loan_total_principal(Core::__Body_Request()["loan_id"]) - GeneralControl::_Loan_total_interest_waived(Core::__Body_Request()["loan_id"]));
                } else {
                    return (GeneralControl::_Loan_total_penalty(Core::__Body_Request()["loan_id"], Core::__Body_Request()["due_date"]) + GeneralControl::_Loan_total_fees(Core::__Body_Request()["loan_id"],
                            Core::__Body_Request()["due_date"]) + GeneralControl::_Loan_total_interest(Core::__Body_Request()["loan_id"], Core::__Body_Request()["due_date"]) + GeneralControl::_Loan_total_principal(Core::__Body_Request()["loan_id"],
                            Core::__Body_Request()["due_date"]) - GeneralControl::_Loan_total_interest_waived(Core::__Body_Request()["loan_id"], Core::__Body_Request()["due_date"]));

                }
          
            }
         public static function
                                 _Loan_total_penalty($id="", $due_date = "")
            {
                if (empty($due_date)) {
                        $condition1 = array(
                                "select" => "SUM(penalty) as penalty",
                                "where" => ["loan_schedules.loan_id" => $id],
                                "join" => array(
                                        "borrowers"     => "borrowers.id = loan_schedules.borrower_id",
                                        "loans" => "loans.id = loan_schedules.loan_id"
                                ),
                               
                             );
                        $loan_total_penalty = RapidDataModel::read('loan_schedules', $condition1)["rows"];
                        return $loan_total_penalty[0]['penalty'];

                       
                } else {
                    

                    $condition1 = array(
                                "select" => "SUM(penalty) as penalty",
                                "where" => ["loan_schedules.loan_id" => $id,
                                            "loan_schedules.due_date <" => $due_date
                                            ],
                                "join" => array(
                                        "borrowers"     => "borrowers.id = loan_schedules.borrower_id",
                                        "loans" => "loans.id = loan_schedules.loan_id"
                                ),
                               
                             );
                    $loan_total_penalty = RapidDataModel::read('loan_schedules', $condition1)["rows"];
                        return $loan_total_penalty[0]['penalty'];

                    
                }
            //      $x =   GeneralControl::_Loan_total_fees(Core::__Body_Request()["loan_id"],Core::__Body_Request()["due_date"]);
            // print_r($x);
            }
         public static function 
                                _Loan_total_fees($id="", $due_date = "")
            {
                 if (empty($due_date)) {
                        $condition1 = array(
                                "select" => "SUM(fees) as fees",
                                "where" => ["loan_schedules.loan_id" => $id],
                                "join" => array(
                                        "borrowers"     => "borrowers.id = loan_schedules.borrower_id",
                                        "loans" => "loans.id = loan_schedules.loan_id"
                                ),
                               
                             );
                        $loan_total_fees = RapidDataModel::read('loan_schedules', $condition1)["rows"];
                        return $loan_total_fees[0]['fees'];

                       
                } else {


                    $condition1 = array(
                                "select" => "SUM(fees) as fees",
                                "where" => ["loan_schedules.loan_id" => $id,
                                            "loan_schedules.due_date <" => $due_date
                                            ],
                                "join" => array(
                                        "borrowers"     => "borrowers.id = loan_schedules.borrower_id",
                                        "loans" => "loans.id = loan_schedules.loan_id"
                                ),
                               
                             );
                    $loan_total_fees = RapidDataModel::read('loan_schedules', $condition1)["rows"];
                        return $loan_total_fees[0]['fees'];

                    
                }
            // $x =   GeneralControl::_Loan_total_interest(Core::__Body_Request()["loan_id"],Core::__Body_Request()["due_date"]);
            // print_r($x);

            }
         public static function _Loan_total_interest($id="", $due_date = "")
            {
                if (empty($due_date)) {
                        $condition1 = array(
                                "select" => "SUM(interest) as interest",
                                "where" => ["loan_schedules.loan_id" => $id],
                                "join" => array(
                                        "borrowers"     => "borrowers.id = loan_schedules.borrower_id",
                                        "loans" => "loans.id = loan_schedules.loan_id"
                                ),
                               
                             );
                        $loan_total_interest = RapidDataModel::read('loan_schedules', $condition1)["rows"];
                        return $loan_total_interest[0]['interest'];
                       
                } else {


                    $condition1 = array(
                                "select" => "SUM(interest) as interest",
                                "where" => ["loan_schedules.loan_id" => $id,
                                            "loan_schedules.due_date <" => $due_date
                                            ],
                                "join" => array(
                                        "borrowers"     => "borrowers.id = loan_schedules.borrower_id",
                                        "loans" => "loans.id = loan_schedules.loan_id"
                                ),
                               
                             );
                    $loan_total_interest = RapidDataModel::read('loan_schedules', $condition1)["rows"];
                    return $loan_total_interest[0]['interest'];

                    
                }
            // $x =   GeneralControl::_Loan_total_principal(Core::__Body_Request()["loan_id"],Core::__Body_Request()["due_date"]);
            // print_r($x);
            }
         public static function 
                                _Loan_total_principal($id="", $due_date = "")
            {
                if (empty($due_date)) {
                        $condition1 = array(
                                "select" => "SUM(loan_schedules.principal) as principal",
                                "where" => ["loan_schedules.loan_id" => $id],
                                "join" => array(
                                        "borrowers"     => "borrowers.id = loan_schedules.borrower_id",
                                        "loans" => "loans.id = loan_schedules.loan_id"
                                ),
                               
                             );
                        $loan_total_principal= RapidDataModel::read('loan_schedules', $condition1)["rows"];
                        return $loan_total_principal[0]['principal'];

                       
                } else {


                    $condition1 = array(
                                "select" => "SUM(loan_schedules.principal) as principal",
                                "where" => ["loan_schedules.loan_id" => $id,
                                            "loan_schedules.due_date <" => $due_date
                                            ],
                                "join" => array(
                                        "borrowers"     => "borrowers.id = loan_schedules.borrower_id",
                                        "loans" => "loans.id = loan_schedules.loan_id"
                                ),
                               
                             );
                    $loan_total_principal = RapidDataModel::read('loan_schedules', $condition1)["rows"];
                    return $loan_total_principal[0]['principal'];
                   
                }



// $x = GeneralControl::_Loan_total_interest_waived( Core::__Body_Request()["loan_id"],Core::__Body_Request()["due_date"]);
// print_r($x);
            }
         public static function _Loan_total_interest_waived($id="", $due_date = "")
            {
                if (empty($due_date)) {
                        $condition1 = array(
                                "select" => "SUM(interest_waived) as interest_waived",
                                "where" => ["loan_schedules.loan_id" => $id],
                                "join" => array(
                                        "borrowers"     => "borrowers.id = loan_schedules.borrower_id",
                                        "loans" => "loans.id = loan_schedules.loan_id"
                                ),
                               
                             );
                        $loan_total_interest_waived= RapidDataModel::read('loan_schedules', $condition1)["rows"];
                        return $loan_total_interest_waived[0]['interest_waived'];
                        
                } else {


                    $condition1 = array(
                                "select" => "SUM(interest_waived) as interest_waived",
                                "where" => ["loan_schedules.loan_id" => $id,
                                            "loan_schedules.due_date <" => $due_date
                                            ],
                                "join" => array(
                                        "borrowers"     => "borrowers.id = loan_schedules.borrower_id",
                                        "loans" => "loans.id = loan_schedules.loan_id"
                                ),
                               
                             );
                    $loan_total_interest_waived = RapidDataModel::read('loan_schedules', $condition1)["rows"];
                  
                    return $loan_total_interest_waived[0]['interest_waived'];
                  
                }
            }

 }